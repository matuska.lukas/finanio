/**
 * The entry point of the Finanio app
 * @author Sili, Jan Svabik (jan.svabik@sili.dev)
 * @version 1.0
 * @see https://sili.dev/cs/
 */

// config as global variable
global.CONFIG = require('./config');

// load the server plugin (Express)
const express = require('express');
const app = express();

// load some libraries
const moment = require('moment');
const path = require('path');
const bodyparser = require('body-parser');

// set extended urlencoded to true (post)
app.use(bodyparser.urlencoded({extended: true}));

// set up views directory and the rendering engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// set serving static files from the static dir
app.use(express.static(path.join(__dirname, 'static')));

// routers
const main = require('./routes/main');
app.use('/', main);

// run the server
app.listen(CONFIG.port, () => {
    console.log(moment().format('YYYY-MM-DD HH:mm:ss') + ' Listening on port ' + CONFIG.port + ' (My first Node.js app)');
});