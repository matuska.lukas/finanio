/**
 * Financial movement database model
 * @author Jan Svabik (jan.svabik@sili.dev)
 * @version 1.0
 */

// library for easy database manipulations
const mongoose = require('../components/db');

// the schema itself
var financialMovementSchema = new mongoose.Schema({
    name: String,
    date: Date,
    category: String,
    value:{
        amount: Number,
        currency: String,
    },
    recordType:{
        type:String,
        enum:['income', 'expenditure', 'claim'],
    }
});

// export
module.exports = mongoose.model('FinancialMovement', financialMovementSchema, 'financialMovement');
