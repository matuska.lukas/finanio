module.exports = {
    // location
    protocol: 'http',
    url: 'localhost',
    port: 3001,

    // full url
    fullUrl: this.protocol + '://' + this.url + (String(this.port).length > 0 ? ':' + this.port : ''),

    // database credentials
    db: {
        host: 'localhost',
        name: 'finanio',
        user: 'finanio',
        password: '1234',
        port: 27018,
    },

    // categories
    categories: [
        'Nezařazeno',
        'Cestování',
        'Jídlo a pití',
        'Investice',
        'Služby',
        'Blbosti',
    ],

    // currency
    currencies: [
        'CZK',
        'USD',
        'EUR',
    ],
};
