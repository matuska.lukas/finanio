/**
 * The main router of the app
 * @author Jan Svabik (jan.svabik@sili.dev)
 * @version 1.0
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Libraries
 */
const moment = require('moment');
const numberFormat = require('../libs/numberFormat');

/**
 * Controllers
 */
const analyticsController = require('../controllers/analytics');
const dashboardController = require('../controllers/dashboard');
const movementController = require('../controllers/movement');

/**
 * Routes
 */

// set local variables
router.all('/*', (req, res, next) => {
    res.locals = {
        currentPath: req.originalUrl,
        moment: moment,
        numberFormat: numberFormat,
    };

    // move to the next route
    next();
});

// redirect from / to the product list
router.get('/', (req, res) => {
    res.redirect('/dashboard/');
});

/**
 ** DASHBOARD
 */

// display the dashboard
router.get('/dashboard', (req, res) => {
    dashboardController.view(req, res);
});

/**
 ** FINANCIAL MOVEMENTS
 */

// list all movements
router.get('/fin', (req, res) => {
    movementController.viewAll(req, res);
});

// display the page for the complex adding
router.get('/fin/add', (req, res) => {
    movementController.viewAddPage(req, res);
});

// save new movement
router.post('/fin/add', (req, res) => {
    movementController.save(req, res);
});

// delete movement
router.get('/fin/delete/:id', (req, res) => {
    movementController.delete(req, res);
});

// display all claims
router.get('/fin/claims', (req, res) => {
    movementController.viewClaims(req, res);
});

// display data from this month only
router.get('/fin/this-month', (req, res) => {
    movementController.viewThisMonthData(req, res);
});

/**
 ** ANALYTICS
 */

// display analytics by categories
router.get('/analytics/categories', (req, res) => {
    analyticsController.byCategory(req, res);
});

// display the record with the biggest value
router.get('/analytics/oh-my-god', (req, res) => {
    analyticsController.theBiggestHole(req, res);
});

// display monthly analysis
router.get('/analytics/monthly', (req, res) => {
    analyticsController.monthly(req, res);
});

/**
 * Export the router
 */
module.exports = router;