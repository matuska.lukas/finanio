/**
 * Movements controller
 * @author Jan Svabik (jan.svabik@sili.dev)
 * @version 1.0
 */

 /**
  * Libs
  */
const moment = require('moment');

/**
 * Models
 */
const FiniancialMovement = require('../models/FinancialMovement');
// display all financial movements
module.exports.viewAll = (req, res) => {
    FiniancialMovement.find({}, (err, financialMovements) => {
        if (err) {
            return console.errror(err);
        }
        
        res.render('movement-list', {
            financialMovements: financialMovements,
        });
    });
};

// save new financial movement
module.exports.save = (req, res) => {
    let fm = {
        name: req.body.name,
        date: moment(req.body.date),
        category: req.body.category,
        value:{
            amount: req.body.value,
            //amount: 100,
            currency: req.body.currency,
            //currency: 'CZK',
        },
        recordType: req.body.type,
    };

    new FiniancialMovement(fm).save( (err) => {
        if (err){
            return console.error(err);
        }

        res.redirect('/fin/');
    });

};

// delete movement by id
module.exports.delete = (req, res) => {
    FiniancialMovement.deleteOne({
        _id: req.params.id,
    }, (err) => {
        if(err){
            return console.error(err);
        }
    })
    // redirect back
    res.redirect(req.get("Referer"));
}

// display page for adding new 
module.exports.viewAddPage = (req, res) => {
    res.render('movement-add');
};

// display all claims
module.exports.viewClaims = (req, res) => {
    // TODO Zatim neni implementovano
    res.render('movement-claim-list');
};

// display all data from this month
module.exports.viewThisMonthData = (req, res) => {
    // TODO Zatim neni implementovano
    res.render('movement-this-month');
};
