/**
 * Analytics controller
 * @author Jan Svabik (jan.svabik@sili.dev)
 * @version 1.0
 */

// display analytics by categories
module.exports.byCategory = (req, res) => {
    // TODO Zatim neni implementovano
    res.render('analytics-categories');
};

// display the record with the biggest value
module.exports.theBiggestHole = (req, res) => {
    // TODO Zatim neni implementovano
    res.render('analytics-biggest-value');
};

// display the record with the biggest value
module.exports.monthly = (req, res) => {
    // create array with 12 items and set these items to 0
    let months = new Array(12).fill(0);

    // TODO Zatim neni implementovano

    res.render('analytics-monthly');
};